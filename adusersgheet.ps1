#Install and import module prereqs

#Install-PackageProvider NuGet -Force
#Set-PSRepository PSGallery -InstallationPolicy Trusted
#Install-Module UMN-Google -Confirm:$False -Force
Import-Module UMN-Google
Import-Module ActiveDirectory

# Set security protocol to TLS 1.2 to avoid TLS errors
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Google API Authozation
# CERT NEEDS TO BE PLACED ON SVR IN COMMON LOCATION e.g. C:\cert.p12
$scope = "https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.file"
$certPath = "C:\gcert.p12"
$iss = 'svcacc1@ps-ad-gtest.iam.gserviceaccount.com'
$certPswd = 'notasecret'
$accessToken = Get-GOAuthTokenService -scope $scope -certPath $certPath -certPswd $certPswd -iss $iss
  
#Set-GFilePermissions -accessToken $accessToken -fileID $SpreadsheetID -role writer -type user -emailAddress 'steven.kranston@shapeint.com'

# Point to spreadsheet
$SpreadsheetID = '1wlF1L7PJm35Cjz4HWGvsfkX6v30IEktqcJh7UMcrAz8'

#$Title = 'AD AUDIT GOOGLE TEST'
#$SpreadsheetID = (New-GSheetSpreadSheet -accessToken $accessToken -title $Title).spreadsheetId
#$SpreadsheetID


# queries DC for all AD users in users container, excludes non-human/admin accounts
$usrCont = Get-ADDomain | Select-Object -Property UsersContainer
$domainName = [System.Net.Dns]::GetHostName() | Out-String
$import = [System.Collections.ArrayList]::new()
# add column headings
$import.Add( @("Name", "Username", "Last Logon", "Group Membership", "Enabled", "VPN Permitted")) | Out-Null

# query AD for list of users along with desired attributes
$rawdata = Get-ADUser -Filter * -SearchBase $usrCont.UsersContainer -Properties LastLogonDate, MemberOf, Enabled, msNPAllowDialin |
 #Where-Object { $_.Name -notlike "*scanner*" -and $_.Name -notlike "*scanning*" -and $_.Name -notlike "*admin*" -and $_.Name -notlike "*QB*"-and $_.Name -notlike "*default*" } |
  Select-Object -Property Name, UserPrincipalName, LastLogonDate, Enabled, msNPAllowDialin, @{name="MemberOf";expression={$_.memberof -join ","}}

# Create new sheet
#$Sheet = $import.count
#Add-GSheetSheet -accessToken $accessToken -sheetName $domainName -spreadSheetID $SpreadsheetID

$memberOfRegexSearch = 'CN\=(.*?)\,'

$rawdata | ForEach-Object {
    $cleanName = $_.Name
    $cleanUPN = $_.UserPrincipalName
    if ( $_.LastLogonDate -eq $null ) { $cleanLastLogon = "NEVER" } else { $cleanLastLogon = $_.LastLogonDate.ToString() } 
    $cleanMemberOf = Select-String $memberOfRegexSearch -InputObject $_.MemberOf -AllMatches -CaseSensitive | Foreach {$_.matches.Value}
    $cleanEnabled = $_.Enabled
    if ( $_.msNPAllowDialin -eq $null ) { $cleanVPNPermitted = "FALSE" } else { $cleanVPNPermitted = $_.msNPAllowDialin } 
    #Write-Host @($cleanName, $cleanUPN, $cleanLastLogon, $cleanMemberOf, $cleanEnabled, $cleanVPNPermitted)
    $import.Add( @($cleanName, $cleanUPN, $cleanLastLogon, $cleanMemberOf, $cleanEnabled, $cleanVPNPermitted) ) | Out-Null
    }

#$rawdata | ForEach-Object { Select-String $memberOfRegexSearch -InputObject $_.MemberOf -AllMatches -CaseSensitive | Foreach {$_.matches.Value} } | Write-Host
# Upload data to Google Sheets with Set-GSheetData
Set-GSheetData -accessToken $accessToken -append -sheetName $domainName -spreadSheetID $SpreadsheetID -values $import -Debug -Verbose

#-rangeA1 "A1:D$($import.Count)"


#(if ($_.msNPAllowDialin -eq $null {return $false} else $_.msNPAllowDialin))